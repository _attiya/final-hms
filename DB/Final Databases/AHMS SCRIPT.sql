/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Bed]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bed](
	[bedid] [int] IDENTITY(1,1) NOT NULL,
	[bedcharges] [int] NULL,
	[roomid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[bedid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[CallOnDoctor]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CallOnDoctor](
	[callid] [int] IDENTITY(1,1) NOT NULL,
	[charges] [int] NOT NULL,
	[docno] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[callid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Department]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Department](
	[deptno] [int] IDENTITY(1,1) NOT NULL,
	[deptname] [varchar](255) NOT NULL,
	[depfac] [varchar](255) NOT NULL,
	[deploc] [varchar](255) NOT NULL,
	[Hospitalid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[deptno] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Discharge]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Discharge](
	[disid] [int] IDENTITY(1,1) NOT NULL,
	[balancecharges] [int] NULL,
	[testcharges] [int] NULL,
	[bloodcharges] [int] NULL,
	[payment] [int] NULL,
	[dateofdischarge] [datetime] NULL,
	[operationscharges] [int] NULL,
	[doccharges] [int] NULL,
	[treatmentadvice] [varchar](255) NULL,
	[indoorpid] [int] NULL,
	[outdoorpid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[disid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Doctor]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Doctor](
	[docno] [int] IDENTITY(1,1) NOT NULL,
	[deptno] [int] NULL,
	[docname] [varchar](255) NOT NULL,
	[addressdoc] [varchar](255) NOT NULL,
	[qulifications] [varchar](255) NOT NULL,
	[specialization] [varchar](255) NOT NULL,
	[contactno] [varchar](255) NOT NULL,
	[hospitalid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[docno] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Hospital]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hospital](
	[cstno] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[addressHospital] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[cstno] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Inpatient]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inpatient](
	[Pid] [int] IDENTITY(1,1) NOT NULL,
	[pono] [int] NULL,
	[advancepayment] [int] NULL,
	[roomno] [int] NULL,
	[labno] [int] NULL,
	[medid] [int] NULL,
	[nurseid] [int] NULL,
 CONSTRAINT [PK__Inpatien__C57059381FCDBCEB] PRIMARY KEY CLUSTERED 
(
	[Pid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[LabReport]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LabReport](
	[labno] [int] IDENTITY(1,1) NOT NULL,
	[pno] [int] NULL,
	[amount] [int] NULL,
	[category] [varchar](50) NULL,
	[dateofLab] [datetime] NULL,
	[docno] [int] NULL,
	[weightofpatient] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[labno] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[MedicalStore]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalStore](
	[medicalstoreid] [int] IDENTITY(1,1) NOT NULL,
	[medid] [int] NULL,
	[name] [varchar](255) NULL,
	[addressmedialstore] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[medicalstoreid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Medicine]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Medicine](
	[medid] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[expiredatemed] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[medid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Nurse]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Nurse](
	[nurseid] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[qulifications] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[nurseid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Operation]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operation](
	[operationtheaterno] [int] IDENTITY(1,1) NOT NULL,
	[docno] [int] NULL,
	[patientcondition] [varchar](255) NULL,
	[typeofoperation] [varchar](255) NULL,
	[treatment] [varchar](255) NULL,
	[inpaitentid] [int] NULL,
 CONSTRAINT [PK__Operatio__4EE2435427DC96FD] PRIMARY KEY CLUSTERED 
(
	[operationtheaterno] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[outpaitent]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[outpaitent](
	[putpid] [int] IDENTITY(1,1) NOT NULL,
	[pono] [int] NULL,
	[labno] [int] NULL,
	[dateofadmit] [datetime] NULL,
	[dateofdiscarge] [datetime] NULL,
	[medid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[putpid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Patient]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patient](
	[pono] [int] IDENTITY(1,1) NOT NULL,
	[pname] [varchar](255) NOT NULL,
	[addresspatient] [varchar](255) NOT NULL,
	[age] [int] NULL,
	[gender] [varchar](255) NULL,
	[city] [varchar](255) NULL,
	[contactno] [varchar](255) NULL,
	[docid] [int] NULL,
	[InOutCheck] [bit] NOT NULL,
 CONSTRAINT [PK__Patient__41A5B96200551192] PRIMARY KEY CLUSTERED 
(
	[pono] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Registeration]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Registeration](
	[regno] [int] IDENTITY(1,1) NOT NULL,
	[pno] [int] NULL,
	[regdate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[regno] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[RegularDoctor]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegularDoctor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[paymentcharges] [int] NOT NULL,
	[docno] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Room]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room](
	[roomid] [int] IDENTITY(1,1) NOT NULL,
	[wardid] [int] NULL,
	[roomcharges] [int] NULL,
	[statusofroom] [bit] NULL,
	[type] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[roomid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Staff]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[staffid] [int] IDENTITY(1,1) NOT NULL,
	[docid] [int] NULL,
	[nurseid] [int] NULL,
 CONSTRAINT [PK__Staff__645AE4A62F10007B] PRIMARY KEY CLUSTERED 
(
	[staffid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Stock]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stock](
	[sno] [int] IDENTITY(1,1) NOT NULL,
	[medicalstoreid] [int] NULL,
	[quantity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[sno] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Test]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Test](
	[testid] [int] IDENTITY(1,1) NOT NULL,
	[docno] [int] NULL,
	[name] [varchar](255) NULL,
	[amount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[testid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
/****** Object:  Table [dbo].[Ward]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ward](
	[wardid] [int] IDENTITY(1,1) NOT NULL,
	[wardname] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[wardid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8121befb-dca2-4f8c-9378-ada0605517fb', N'attiyahussain47@gmail.com', 0, N'AOL99NL8AD5n5T2wzh6yBroVWsmoU2y2naqIt7QZJ+oX0u5YbcColuOzovQ3VWWCrg==', N'2e184264-8352-4b70-8c75-07978c995a68', NULL, 0, 0, NULL, 1, 0, N'attiyahussain47@gmail.com')
SET IDENTITY_INSERT [dbo].[Bed] ON 

INSERT [dbo].[Bed] ([bedid], [bedcharges], [roomid]) VALUES (1, 500, 1)
INSERT [dbo].[Bed] ([bedid], [bedcharges], [roomid]) VALUES (4, 1500, 2)
INSERT [dbo].[Bed] ([bedid], [bedcharges], [roomid]) VALUES (5, 2500, 4)
INSERT [dbo].[Bed] ([bedid], [bedcharges], [roomid]) VALUES (6, 3500, 4)
INSERT [dbo].[Bed] ([bedid], [bedcharges], [roomid]) VALUES (7, 4500, 3)
INSERT [dbo].[Bed] ([bedid], [bedcharges], [roomid]) VALUES (8, 5000, 1)
SET IDENTITY_INSERT [dbo].[Bed] OFF
SET IDENTITY_INSERT [dbo].[CallOnDoctor] ON 

INSERT [dbo].[CallOnDoctor] ([callid], [charges], [docno]) VALUES (1, 2000, 5)
INSERT [dbo].[CallOnDoctor] ([callid], [charges], [docno]) VALUES (2, 2500, 1)
INSERT [dbo].[CallOnDoctor] ([callid], [charges], [docno]) VALUES (4, 1500, 8)
SET IDENTITY_INSERT [dbo].[CallOnDoctor] OFF
SET IDENTITY_INSERT [dbo].[Department] ON 

INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (1, N' Obstetrics & Gynecology', N'250', N'Faisal Town Usmani Road, Lahore', 1)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (2, N'GASTROENTEROLOGY', N'222', N'Faisal Town Usmani Road, Lahore', 1)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (3, N'ORTHOPEDIC SURGERY', N'200', N'Faisal Town Usmani Road, Lahore', 1)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (4, N' GENERAL SURGERY', N'200', N'Faisal Town Usmani Road, Lahore', 1)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (6, N' DIALYSIS', N'220', N'Service Rd,Jail Road, Lahore', 12)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (7, N' ENDOSCOPY', N'220', N'Service Rd,Jail Road, Lahore', 12)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (8, N' ULTRASOUND', N'202', N'	Service Rd,Jail Road, Lahore', 12)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (9, N' VACCINATION CENTER', N'220', N'Shara-I-Fatima Jinnah, Queen''s Road, Lahore', 11)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (10, N' PULMONOLOGY', N'68', N'Shaikh Zayed Medical Complex, University Avenue, New Muslim Town, Lahore', 13)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (11, N' PSYCHIATRY', N'333', N'Shadman Road, Lahore', 15)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (12, N' PEDIATRICS', N'330', N'Main Boulevard Gulberg, Lahore', 17)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (14, N'Dental Guild', N'400', N'Service Rd,Jail Road, Lahore', 12)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (15, N' PHYSIOTHERAPHY', N'220', N'Faisal Town Usmani Road, Lahore', 1)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (17, N' DENTAL CLINIC', N'200', N'Faisal Town Usmani Road, Lahore', 1)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (18, N' UROLOGY', N'100', N'Faisal Town Usmani Road, Lahore', 1)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (19, N'Ear, Nose & Throat (ENT)', N'300', N'Ferozpur Road, Near Camp Jail,Lahore', 3)
INSERT [dbo].[Department] ([deptno], [deptname], [depfac], [deploc], [Hospitalid]) VALUES (20, N' RADIOGRAPHY', N'130', N'Hospital Road,Lahore', 4)
SET IDENTITY_INSERT [dbo].[Department] OFF
SET IDENTITY_INSERT [dbo].[Discharge] ON 

INSERT [dbo].[Discharge] ([disid], [balancecharges], [testcharges], [bloodcharges], [payment], [dateofdischarge], [operationscharges], [doccharges], [treatmentadvice], [indoorpid], [outdoorpid]) VALUES (1, 200, 400, 500, 500, CAST(N'2017-11-17 00:00:00.000' AS DateTime), 4000, 4000, N'avoid oily food', 2, NULL)
INSERT [dbo].[Discharge] ([disid], [balancecharges], [testcharges], [bloodcharges], [payment], [dateofdischarge], [operationscharges], [doccharges], [treatmentadvice], [indoorpid], [outdoorpid]) VALUES (2, 300, 300, 500, 200, CAST(N'2017-10-10 00:00:00.000' AS DateTime), 4000, 3000, N'control suger level', 4, NULL)
INSERT [dbo].[Discharge] ([disid], [balancecharges], [testcharges], [bloodcharges], [payment], [dateofdischarge], [operationscharges], [doccharges], [treatmentadvice], [indoorpid], [outdoorpid]) VALUES (3, 500, 300, 300, 400, CAST(N'2017-09-14 00:00:00.000' AS DateTime), 3000, 2000, N'control suger level', 4, NULL)
INSERT [dbo].[Discharge] ([disid], [balancecharges], [testcharges], [bloodcharges], [payment], [dateofdischarge], [operationscharges], [doccharges], [treatmentadvice], [indoorpid], [outdoorpid]) VALUES (4, 500, 300, 300, 400, CAST(N'2017-09-14 00:00:00.000' AS DateTime), 3000, 2000, N'control suger level', 4, NULL)
INSERT [dbo].[Discharge] ([disid], [balancecharges], [testcharges], [bloodcharges], [payment], [dateofdischarge], [operationscharges], [doccharges], [treatmentadvice], [indoorpid], [outdoorpid]) VALUES (5, 500, 300, 300, 400, CAST(N'2017-09-14 00:00:00.000' AS DateTime), 3000, 2000, N'control suger level', NULL, 1)
INSERT [dbo].[Discharge] ([disid], [balancecharges], [testcharges], [bloodcharges], [payment], [dateofdischarge], [operationscharges], [doccharges], [treatmentadvice], [indoorpid], [outdoorpid]) VALUES (6, 200, 300, 400, 400, CAST(N'2017-08-31 00:00:00.000' AS DateTime), 3000, 4000, N'control suger level', 2, NULL)
INSERT [dbo].[Discharge] ([disid], [balancecharges], [testcharges], [bloodcharges], [payment], [dateofdischarge], [operationscharges], [doccharges], [treatmentadvice], [indoorpid], [outdoorpid]) VALUES (7, 100, 100, 100, 200, CAST(N'2017-11-13 00:00:00.000' AS DateTime), 1000, 4000, N'TAke Milk', 5, NULL)
INSERT [dbo].[Discharge] ([disid], [balancecharges], [testcharges], [bloodcharges], [payment], [dateofdischarge], [operationscharges], [doccharges], [treatmentadvice], [indoorpid], [outdoorpid]) VALUES (8, 100, 100, 100, 200, CAST(N'2017-11-01 00:00:00.000' AS DateTime), 3000, 4000, N'eat healthy', 5, NULL)
SET IDENTITY_INSERT [dbo].[Discharge] OFF
SET IDENTITY_INSERT [dbo].[Doctor] ON 

INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (1, 14, N'Dr. Salman Athar Qurreshi', N'Behria Town, Lahore', N'MBBS, FCPS', N'Anaesthetist', N'03007689134', 12)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (2, 18, N'Dr. Atif Ali', N'Faisal Town Usmani Road, Lahore', N'F.c.p.s, M.B.B.S', N'Urologist', N'03004758962', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (3, 15, N'Dr. shabana azeem', N'Johar Town, Lahore', N'FCPS, MBBS	', N'General Physician', N'034456783291', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (4, 3, N'Dr. Mohammad Arif', N'Allama Iqbal Town, Lahore', N'MBBS.MCPS.FCPS	', N'Orthopedic Surgeon', N'03004578123', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (5, 8, N'Dr. ghazala Kashif', N'Allama Iqbal Town, Lahore', N'MBBS, Diploma in Ultra Sound from JPMC Karachi', N'sonologist', N'03343617654', 12)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (6, 17, N'Dr. Shoaib Kazi', N'Muslim Town, Lahore', N'B.D.S, B.Sc, FCPS	', N'Dental Surgeon', N'03445978623', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (7, 19, N'Dr. Rasheed Hameed', N'Johar Town, Lahore', N'MBBS,MCPS ,MS	', N'ENT Surgeon', N'03441298735', 3)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (8, 20, N'DR. HAROON SAEED', N'Muslim Town,Lahore', N'MBBS. FRCR II (UK). FCPS (PAK).', N'Radiologist', N' (+92-42) 5869660 / 5160107', 4)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (9, 4, N'Dr. Ch Mohammad Kamran', N'Muslim Town, Lahore', N'MBBS FCPS', N'General Surgeon', N'03131234567', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (10, 1, N'DR. SHAHEEN KAUSAR MALIK', N'Bahria Town, Lahore', N'MBBS', N'Gynecologist', N' (+92-42) 5869660 / 5160107', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (11, 2, N'DR. M. AZHAR CHAUDHRY', N'DHA, Lahore', N'MBBS MCP. FCPS - Gasro-Enterologist', N'Gastroenterologist', N'03130987654', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (12, 3, N'DR. IMRAN NAUSHER', N'Samnabad,Lahore', N'MBBS (AIMC) , FCPS (ORTH)', N'Orthopedic Surgeon', N'03000129857', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (13, 15, N'DR SAHRISH AHMAD', N'DHA, Lahore', N'MS Clinical Psychology', N'Psychologist', N'03001298736', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (14, 4, N'DR MUHAMMAD RAFEEQ SHAH', N'Jahanzaib block, Allama Iqbal Town,Lahore', N'm.b.b.s F.C.P.S (resident)', N' Cardiologist', N'03004598125', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (15, 12, N'DR AZAM KHALIQ', N'Rehmat Square, Aisha Manzil, Block-7, F.B.Area, Karachi', N'M.B. B. S D. C. H', N'Child Specialist', N'02136337054', 1)
INSERT [dbo].[Doctor] ([docno], [deptno], [docname], [addressdoc], [qulifications], [specialization], [contactno], [hospitalid]) VALUES (16, 12, N'DR. MUBINA AGBOATWALLA', N'Jahanzaib block ,Allama Iqbal Town, Lahore', N'M.B.B.S, DCH, MCPS (PAEDS)', N'Child Specialist', N'0335 4145157', 1)
SET IDENTITY_INSERT [dbo].[Doctor] OFF
SET IDENTITY_INSERT [dbo].[Hospital] ON 

INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (1, N'Jinnah Hospital Lahore', N'Faisal Town Usmani Road, Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (2, N'Lady Willingdon Hospital', N' Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (3, N' General Hospital', N'Ferozpur Road, Near Camp Jail,Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (4, N'Mayo Hospital', N'Hospital Road,Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (6, N'Punjab Institute of Cardiology', N' Jail Rd, Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (7, N'Punjab Social Security Health Management Company, Hospital', N'Manga - Raiwind Road, Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (8, N'Services Hospital', N'Ghaus-ul-Azam, Jail Road, Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (9, N'Shalamar Hospital', N' 2 Shalimar Link Road,Lahore ')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (10, N'Punjab Institute of Mental Health', N'Jail Rd, Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (11, N'Sir Ganga Ram Hospital', N'Shara-I-Fatima Jinnah, Queen''s Road, Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (12, N'Omar Hospital', N'Service Rd,Jail Road, Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (13, N'Shaikh Zayed Hospital', N'Shaikh Zayed Medical Complex, University Avenue, New Muslim Town? Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (14, N'Mian Munshi Hospital Lahore ', N'Sagian Wala Bypass, Lahore ')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (15, N'Fatima Memorial Hospital', N'Shadman Road, Lahore')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (16, N'Suraya Azeem Hospital', N' Bahawalpur Rd, Samanabad Town, Lahore, Punjab')
INSERT [dbo].[Hospital] ([cstno], [name], [addressHospital]) VALUES (17, N'The Children''s Hospital', N'Main Boulevard Gulberg, Lahore')
SET IDENTITY_INSERT [dbo].[Hospital] OFF
SET IDENTITY_INSERT [dbo].[Inpatient] ON 

INSERT [dbo].[Inpatient] ([Pid], [pono], [advancepayment], [roomno], [labno], [medid], [nurseid]) VALUES (1, 1, 2000, 1, 3, 11, 7)
INSERT [dbo].[Inpatient] ([Pid], [pono], [advancepayment], [roomno], [labno], [medid], [nurseid]) VALUES (2, 2, 3000, 1, 3, 11, 7)
INSERT [dbo].[Inpatient] ([Pid], [pono], [advancepayment], [roomno], [labno], [medid], [nurseid]) VALUES (3, 1, 500, 1, 2, 1, 1)
INSERT [dbo].[Inpatient] ([Pid], [pono], [advancepayment], [roomno], [labno], [medid], [nurseid]) VALUES (4, 8, 2500, 5, 2, 1, 9)
INSERT [dbo].[Inpatient] ([Pid], [pono], [advancepayment], [roomno], [labno], [medid], [nurseid]) VALUES (5, 3, 500, 1, 7, 3, 2)
INSERT [dbo].[Inpatient] ([Pid], [pono], [advancepayment], [roomno], [labno], [medid], [nurseid]) VALUES (6, 14, 2000, 1, 3, 1, 2)
SET IDENTITY_INSERT [dbo].[Inpatient] OFF
SET IDENTITY_INSERT [dbo].[LabReport] ON 

INSERT [dbo].[LabReport] ([labno], [pno], [amount], [category], [dateofLab], [docno], [weightofpatient]) VALUES (2, 1, 34, N'Injured', CAST(N'2017-11-28 00:00:00.000' AS DateTime), 1, 44)
INSERT [dbo].[LabReport] ([labno], [pno], [amount], [category], [dateofLab], [docno], [weightofpatient]) VALUES (3, 1, 500, N'Blood', CAST(N'2017-11-02 00:00:00.000' AS DateTime), 1, 66)
INSERT [dbo].[LabReport] ([labno], [pno], [amount], [category], [dateofLab], [docno], [weightofpatient]) VALUES (4, 1, 600, N'electrocardiogram (ECG)', CAST(N'2017-10-13 00:00:00.000' AS DateTime), 1, 69)
INSERT [dbo].[LabReport] ([labno], [pno], [amount], [category], [dateofLab], [docno], [weightofpatient]) VALUES (5, 1, 600, N'electrocardiogram (ECG)', CAST(N'2017-10-13 00:00:00.000' AS DateTime), 1, 69)
INSERT [dbo].[LabReport] ([labno], [pno], [amount], [category], [dateofLab], [docno], [weightofpatient]) VALUES (6, 1, 600, N'Injured', CAST(N'2017-11-04 00:00:00.000' AS DateTime), 1, 70)
INSERT [dbo].[LabReport] ([labno], [pno], [amount], [category], [dateofLab], [docno], [weightofpatient]) VALUES (7, 10, 800, N'international normalized ratio (INR)', CAST(N'2017-11-04 00:00:00.000' AS DateTime), 8, 70)
SET IDENTITY_INSERT [dbo].[LabReport] OFF
SET IDENTITY_INSERT [dbo].[MedicalStore] ON 

INSERT [dbo].[MedicalStore] ([medicalstoreid], [medid], [name], [addressmedialstore]) VALUES (1, 1, N'Noor Medical Store', N'whadat road lahore')
INSERT [dbo].[MedicalStore] ([medicalstoreid], [medid], [name], [addressmedialstore]) VALUES (2, 3, N'Al-Shifa Medica Store', N'Allama Iqbal Town, Lahore')
INSERT [dbo].[MedicalStore] ([medicalstoreid], [medid], [name], [addressmedialstore]) VALUES (3, 4, N'Al-Rehman Medical Store', N'JAhanzaib block , Lahore')
SET IDENTITY_INSERT [dbo].[MedicalStore] OFF
SET IDENTITY_INSERT [dbo].[Medicine] ON 

INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (1, N'Aspirin', CAST(N'2017-12-31 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (2, N'Plavix', CAST(N'2018-01-31 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (3, N'Warfarin ', CAST(N'2018-02-14 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (4, N'Bumetanide', CAST(N'2017-12-30 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (5, N'naproxen sodium', CAST(N'2018-01-27 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (6, N'Acetaminophen', CAST(N'2018-01-27 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (7, N'Theophylline', CAST(N'2018-03-14 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (8, N'panadol', CAST(N'2018-01-20 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (9, N'Disprine', CAST(N'2017-11-30 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (10, N'Lomustine ', CAST(N'2018-01-18 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (11, N' Melphalan ', CAST(N'2017-12-30 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (12, N'Fluorouracil ', CAST(N'2018-01-23 00:00:00.000' AS DateTime))
INSERT [dbo].[Medicine] ([medid], [name], [expiredatemed]) VALUES (13, N' Masoprocol ', CAST(N'2017-11-29 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Medicine] OFF
SET IDENTITY_INSERT [dbo].[Nurse] ON 

INSERT [dbo].[Nurse] ([nurseid], [name], [qulifications]) VALUES (1, N'Zainab', N'CRITICAL CARE NURSING COURSE (CCNC)')
INSERT [dbo].[Nurse] ([nurseid], [name], [qulifications]) VALUES (2, N'Zarina', N'IV  Nursing Course')
INSERT [dbo].[Nurse] ([nurseid], [name], [qulifications]) VALUES (4, N'Bisma Rehman', N'B.S Nursing (Generic Four Years)')
INSERT [dbo].[Nurse] ([nurseid], [name], [qulifications]) VALUES (5, N'Fatima Rasheed', N'EMERGENCY NURSE PEDIATRIC COURSE (ENPC)')
INSERT [dbo].[Nurse] ([nurseid], [name], [qulifications]) VALUES (6, N'Fariha Khalid', N'PHLEBOTOMY COURSE')
INSERT [dbo].[Nurse] ([nurseid], [name], [qulifications]) VALUES (7, N'Seema Aziz', N'BASIC AIRWAY MANAGEMENT COURSE')
INSERT [dbo].[Nurse] ([nurseid], [name], [qulifications]) VALUES (8, N'Amna Hussain', N'Certified Emergency Nurse Review Course')
INSERT [dbo].[Nurse] ([nurseid], [name], [qulifications]) VALUES (9, N'Farzana khan', N'TRAUMA NURSING CORE COURSE (TNCC)')
INSERT [dbo].[Nurse] ([nurseid], [name], [qulifications]) VALUES (10, N'1Zarina', N'IV Nursing Course')
SET IDENTITY_INSERT [dbo].[Nurse] OFF
SET IDENTITY_INSERT [dbo].[Operation] ON 

INSERT [dbo].[Operation] ([operationtheaterno], [docno], [patientcondition], [typeofoperation], [treatment], [inpaitentid]) VALUES (1, 8, N'Stable', N'Arthroscopy', N'eat healthy food', 3)
INSERT [dbo].[Operation] ([operationtheaterno], [docno], [patientcondition], [typeofoperation], [treatment], [inpaitentid]) VALUES (2, 1, N'Stable', N'Caesarean Section', N'Give Blood', 5)
INSERT [dbo].[Operation] ([operationtheaterno], [docno], [patientcondition], [typeofoperation], [treatment], [inpaitentid]) VALUES (3, 10, N'Stable', N'Caesarean Section', N'Give Blood', 1)
SET IDENTITY_INSERT [dbo].[Operation] OFF
SET IDENTITY_INSERT [dbo].[outpaitent] ON 

INSERT [dbo].[outpaitent] ([putpid], [pono], [labno], [dateofadmit], [dateofdiscarge], [medid]) VALUES (1, 13, 3, CAST(N'2017-11-07 00:00:00.000' AS DateTime), CAST(N'2017-11-07 00:00:00.000' AS DateTime), 2)
INSERT [dbo].[outpaitent] ([putpid], [pono], [labno], [dateofadmit], [dateofdiscarge], [medid]) VALUES (2, 7, 3, CAST(N'2017-11-02 00:00:00.000' AS DateTime), CAST(N'2017-11-02 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[outpaitent] ([putpid], [pono], [labno], [dateofadmit], [dateofdiscarge], [medid]) VALUES (3, 7, 4, CAST(N'2017-11-13 00:00:00.000' AS DateTime), CAST(N'2017-11-13 00:00:00.000' AS DateTime), 6)
SET IDENTITY_INSERT [dbo].[outpaitent] OFF
SET IDENTITY_INSERT [dbo].[Patient] ON 

INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (1, N'Shekaib Ibrahim', N'Leyarri', 21, N'Male', N'Karachi', N'03004457689', 1, 0)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (2, N'Salman', N'Nelam Block, Allama Iqbal Town', 23, N'Male', N'Lahore', N'03338765493', 13, 1)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (3, N'Razia', N'Ichra', 50, N'Female', N'Lahore', N'03004567912', 13, 1)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (4, N'Humera Faisal', N'Santnagar', 33, N'Female', N'Lahore', N'03004589321', 10, 1)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (5, N'Kainat Qureshi', N'DHA', 15, N'Female', N'Lahore', N'03004589321', 16, 1)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (6, N'Rasheeda Bibi', N'Kachehri', 50, N'Female', N'Lahore', N'03004459876', 14, 1)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (7, N'Mohammad Aslam', N'Ichra', 69, N'Male', N'Lahore', N'03004487641', 7, 1)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (8, N'Noor Hassan', N'Ichra', 22, N'Male', N'Lahore', N'03224867912', 6, 0)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (9, N'Noor Hassan-1', N'Modal Town', 30, N'Male', N'LAhore', N'03004892178', 5, 0)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (10, N'Sanam Baloch', N'DHA', 29, N'Female', N'Lahore', N'03004918276', 1, 0)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (11, N'Saba Kamar', N'Rawalpindi', 30, N'Female', N'Islamabad', N'03002345981', 8, 1)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (12, N'Sabahat Ali', N'Anarkali', 23, N'Female', N'Lahore', N'03442865623', 8, 0)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (13, N'Ali', N'ghari show main bazar ', 20, N'Male', N'lahore', N'090078601', 13, 0)
INSERT [dbo].[Patient] ([pono], [pname], [addresspatient], [age], [gender], [city], [contactno], [docid], [InOutCheck]) VALUES (14, N'Farzana khan', N'Leyari', 33, N'Female', N'Karachi', N'04005678934', 10, 1)
SET IDENTITY_INSERT [dbo].[Patient] OFF
SET IDENTITY_INSERT [dbo].[Registeration] ON 

INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (4, 1, CAST(N'2017-11-15 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (5, 2, CAST(N'2017-11-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (6, 3, CAST(N'2017-11-14 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (7, 4, CAST(N'2017-10-24 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (8, 1, CAST(N'2017-10-31 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (9, 6, CAST(N'2017-06-14 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (10, 7, CAST(N'2017-10-20 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (11, 8, CAST(N'2017-10-24 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (12, 9, CAST(N'2017-08-16 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (13, 10, CAST(N'2017-09-21 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (14, 1, CAST(N'2017-06-15 00:00:00.000' AS DateTime))
INSERT [dbo].[Registeration] ([regno], [pno], [regdate]) VALUES (15, 12, CAST(N'2017-11-09 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Registeration] OFF
SET IDENTITY_INSERT [dbo].[RegularDoctor] ON 

INSERT [dbo].[RegularDoctor] ([id], [paymentcharges], [docno]) VALUES (1, 800, 13)
INSERT [dbo].[RegularDoctor] ([id], [paymentcharges], [docno]) VALUES (2, 500, 15)
INSERT [dbo].[RegularDoctor] ([id], [paymentcharges], [docno]) VALUES (3, 1000, 8)
SET IDENTITY_INSERT [dbo].[RegularDoctor] OFF
SET IDENTITY_INSERT [dbo].[Room] ON 

INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (1, 1, 380, 1, N'VIP Suite')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (2, 1013, 1000, 1, N'Isolation Room')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (3, 1013, 1000, 1, N'Isolation Room')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (4, 1006, 500, 1, N'Intensive Care Unit')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (5, 1, 500, 1, N'Isolation Room')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (6, 1, 500, 1, N'Isolation Room')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (7, 1005, 500, 1, N'Intensive Care Unit')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (8, 1, 3000, 1, N'VIP Suite')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (9, 1, 3000, 0, N'VIP Suite')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (10, 1006, 500, 1, N'Two-Bedded Room')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (11, 1, 3000, 1, N'Isolation Room')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (12, 1, 3000, 1, N'VIP Suite')
INSERT [dbo].[Room] ([roomid], [wardid], [roomcharges], [statusofroom], [type]) VALUES (13, 1008, 1000, 1, N'Single Deluxe Room')
SET IDENTITY_INSERT [dbo].[Room] OFF
SET IDENTITY_INSERT [dbo].[Staff] ON 

INSERT [dbo].[Staff] ([staffid], [docid], [nurseid]) VALUES (1, 1, 7)
INSERT [dbo].[Staff] ([staffid], [docid], [nurseid]) VALUES (2, 2, 2)
INSERT [dbo].[Staff] ([staffid], [docid], [nurseid]) VALUES (3, 4, 4)
INSERT [dbo].[Staff] ([staffid], [docid], [nurseid]) VALUES (4, 5, 5)
INSERT [dbo].[Staff] ([staffid], [docid], [nurseid]) VALUES (5, 16, 10)
INSERT [dbo].[Staff] ([staffid], [docid], [nurseid]) VALUES (6, 9, 5)
SET IDENTITY_INSERT [dbo].[Staff] OFF
SET IDENTITY_INSERT [dbo].[Stock] ON 

INSERT [dbo].[Stock] ([sno], [medicalstoreid], [quantity]) VALUES (1, 1, 22)
INSERT [dbo].[Stock] ([sno], [medicalstoreid], [quantity]) VALUES (2, 2, 44)
INSERT [dbo].[Stock] ([sno], [medicalstoreid], [quantity]) VALUES (3, 2, 44)
INSERT [dbo].[Stock] ([sno], [medicalstoreid], [quantity]) VALUES (4, 2, 44)
INSERT [dbo].[Stock] ([sno], [medicalstoreid], [quantity]) VALUES (5, 2, 44)
INSERT [dbo].[Stock] ([sno], [medicalstoreid], [quantity]) VALUES (6, 2, 44)
SET IDENTITY_INSERT [dbo].[Stock] OFF
SET IDENTITY_INSERT [dbo].[Test] ON 

INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (1, 9, N'Blood Test', 500)
INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (2, 10, N'DNA', 1000)
INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (3, 14, N'HIV test', 1000)
INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (4, 9, N'Fasting plasma glucose (FPG)', 200)
INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (5, 9, N'Oral glucose tolerance test (OGTT)', 200)
INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (6, 9, N' Hemoglobin A1c (A1c) test', 200)
INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (7, 9, N'Random plasma (blood) glucose.', 200)
INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (8, 14, N'international normalized ratio (INR) ', 500)
INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (9, 14, N'electrocardiogram (ECG) test', 100)
INSERT [dbo].[Test] ([testid], [docno], [name], [amount]) VALUES (10, 14, N'chest X-ray, breathing test', 500)
SET IDENTITY_INSERT [dbo].[Test] OFF
SET IDENTITY_INSERT [dbo].[Ward] ON 

INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1, N'Gynee')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (3, N'Dental')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1002, N'Renal')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1003, N'Acute Medical Unit')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1004, N'maternity ward')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1005, N'Adult hearing services')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1006, N'Children''s hearing services')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1007, N'Audiology')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1008, N'nursery')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1009, N'Adult Accident and Emergency')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1010, N'Anaesthetics')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1011, N'Albert Ward')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1012, N'Otolaryngology')
INSERT [dbo].[Ward] ([wardid], [wardname]) VALUES (1013, N'Adelaide Ward')
SET IDENTITY_INSERT [dbo].[Ward] OFF
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Bed]  WITH CHECK ADD  CONSTRAINT [FK_Bed_Room] FOREIGN KEY([roomid])
REFERENCES [dbo].[Room] ([roomid])
GO
ALTER TABLE [dbo].[Bed] CHECK CONSTRAINT [FK_Bed_Room]
GO
ALTER TABLE [dbo].[CallOnDoctor]  WITH CHECK ADD  CONSTRAINT [FK_CallOnDoctor_Doctor] FOREIGN KEY([docno])
REFERENCES [dbo].[Doctor] ([docno])
GO
ALTER TABLE [dbo].[CallOnDoctor] CHECK CONSTRAINT [FK_CallOnDoctor_Doctor]
GO
ALTER TABLE [dbo].[Department]  WITH CHECK ADD  CONSTRAINT [FK_Department_Hospital] FOREIGN KEY([Hospitalid])
REFERENCES [dbo].[Hospital] ([cstno])
GO
ALTER TABLE [dbo].[Department] CHECK CONSTRAINT [FK_Department_Hospital]
GO
ALTER TABLE [dbo].[Discharge]  WITH CHECK ADD  CONSTRAINT [FK_Discharge_Inpatient] FOREIGN KEY([indoorpid])
REFERENCES [dbo].[Inpatient] ([Pid])
GO
ALTER TABLE [dbo].[Discharge] CHECK CONSTRAINT [FK_Discharge_Inpatient]
GO
ALTER TABLE [dbo].[Discharge]  WITH CHECK ADD  CONSTRAINT [FK_Discharge_outpaitent] FOREIGN KEY([outdoorpid])
REFERENCES [dbo].[outpaitent] ([putpid])
GO
ALTER TABLE [dbo].[Discharge] CHECK CONSTRAINT [FK_Discharge_outpaitent]
GO
ALTER TABLE [dbo].[Doctor]  WITH CHECK ADD  CONSTRAINT [FK_Doctor_Department] FOREIGN KEY([deptno])
REFERENCES [dbo].[Department] ([deptno])
GO
ALTER TABLE [dbo].[Doctor] CHECK CONSTRAINT [FK_Doctor_Department]
GO
ALTER TABLE [dbo].[Doctor]  WITH CHECK ADD  CONSTRAINT [FK_Doctor_Hospital] FOREIGN KEY([hospitalid])
REFERENCES [dbo].[Hospital] ([cstno])
GO
ALTER TABLE [dbo].[Doctor] CHECK CONSTRAINT [FK_Doctor_Hospital]
GO
ALTER TABLE [dbo].[Inpatient]  WITH CHECK ADD  CONSTRAINT [FK_Inpatient_LabReport] FOREIGN KEY([labno])
REFERENCES [dbo].[LabReport] ([labno])
GO
ALTER TABLE [dbo].[Inpatient] CHECK CONSTRAINT [FK_Inpatient_LabReport]
GO
ALTER TABLE [dbo].[Inpatient]  WITH CHECK ADD  CONSTRAINT [FK_Inpatient_Medicine] FOREIGN KEY([medid])
REFERENCES [dbo].[Medicine] ([medid])
GO
ALTER TABLE [dbo].[Inpatient] CHECK CONSTRAINT [FK_Inpatient_Medicine]
GO
ALTER TABLE [dbo].[Inpatient]  WITH CHECK ADD  CONSTRAINT [FK_Inpatient_Nurse] FOREIGN KEY([nurseid])
REFERENCES [dbo].[Nurse] ([nurseid])
GO
ALTER TABLE [dbo].[Inpatient] CHECK CONSTRAINT [FK_Inpatient_Nurse]
GO
ALTER TABLE [dbo].[Inpatient]  WITH CHECK ADD  CONSTRAINT [FK_Inpatient_Room] FOREIGN KEY([roomno])
REFERENCES [dbo].[Room] ([roomid])
GO
ALTER TABLE [dbo].[Inpatient] CHECK CONSTRAINT [FK_Inpatient_Room]
GO
ALTER TABLE [dbo].[LabReport]  WITH CHECK ADD  CONSTRAINT [FK_LabReport_Doctor] FOREIGN KEY([docno])
REFERENCES [dbo].[Doctor] ([docno])
GO
ALTER TABLE [dbo].[LabReport] CHECK CONSTRAINT [FK_LabReport_Doctor]
GO
ALTER TABLE [dbo].[LabReport]  WITH CHECK ADD  CONSTRAINT [FK_LabReport_Patient] FOREIGN KEY([pno])
REFERENCES [dbo].[Patient] ([pono])
GO
ALTER TABLE [dbo].[LabReport] CHECK CONSTRAINT [FK_LabReport_Patient]
GO
ALTER TABLE [dbo].[MedicalStore]  WITH CHECK ADD  CONSTRAINT [FK_MedicalStore_Medicine] FOREIGN KEY([medid])
REFERENCES [dbo].[Medicine] ([medid])
GO
ALTER TABLE [dbo].[MedicalStore] CHECK CONSTRAINT [FK_MedicalStore_Medicine]
GO
ALTER TABLE [dbo].[Operation]  WITH CHECK ADD  CONSTRAINT [FK_Operation_Doctor] FOREIGN KEY([docno])
REFERENCES [dbo].[Doctor] ([docno])
GO
ALTER TABLE [dbo].[Operation] CHECK CONSTRAINT [FK_Operation_Doctor]
GO
ALTER TABLE [dbo].[Operation]  WITH CHECK ADD  CONSTRAINT [FK_Operation_Inpatient] FOREIGN KEY([inpaitentid])
REFERENCES [dbo].[Inpatient] ([Pid])
GO
ALTER TABLE [dbo].[Operation] CHECK CONSTRAINT [FK_Operation_Inpatient]
GO
ALTER TABLE [dbo].[outpaitent]  WITH CHECK ADD  CONSTRAINT [FK_outpaitent_LabReport] FOREIGN KEY([labno])
REFERENCES [dbo].[LabReport] ([labno])
GO
ALTER TABLE [dbo].[outpaitent] CHECK CONSTRAINT [FK_outpaitent_LabReport]
GO
ALTER TABLE [dbo].[outpaitent]  WITH CHECK ADD  CONSTRAINT [FK_outpaitent_Medicine] FOREIGN KEY([medid])
REFERENCES [dbo].[Medicine] ([medid])
GO
ALTER TABLE [dbo].[outpaitent] CHECK CONSTRAINT [FK_outpaitent_Medicine]
GO
ALTER TABLE [dbo].[outpaitent]  WITH CHECK ADD  CONSTRAINT [FK_outpaitent_Patient] FOREIGN KEY([pono])
REFERENCES [dbo].[Patient] ([pono])
GO
ALTER TABLE [dbo].[outpaitent] CHECK CONSTRAINT [FK_outpaitent_Patient]
GO
ALTER TABLE [dbo].[Patient]  WITH CHECK ADD  CONSTRAINT [FK_Patient_Doctor] FOREIGN KEY([docid])
REFERENCES [dbo].[Doctor] ([docno])
GO
ALTER TABLE [dbo].[Patient] CHECK CONSTRAINT [FK_Patient_Doctor]
GO
ALTER TABLE [dbo].[Registeration]  WITH CHECK ADD  CONSTRAINT [FK_Registeration_Patient] FOREIGN KEY([pno])
REFERENCES [dbo].[Patient] ([pono])
GO
ALTER TABLE [dbo].[Registeration] CHECK CONSTRAINT [FK_Registeration_Patient]
GO
ALTER TABLE [dbo].[RegularDoctor]  WITH CHECK ADD  CONSTRAINT [FK_RegularDoctor_Doctor] FOREIGN KEY([docno])
REFERENCES [dbo].[Doctor] ([docno])
GO
ALTER TABLE [dbo].[RegularDoctor] CHECK CONSTRAINT [FK_RegularDoctor_Doctor]
GO
ALTER TABLE [dbo].[Room]  WITH CHECK ADD  CONSTRAINT [FK_Room_Ward] FOREIGN KEY([wardid])
REFERENCES [dbo].[Ward] ([wardid])
GO
ALTER TABLE [dbo].[Room] CHECK CONSTRAINT [FK_Room_Ward]
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD  CONSTRAINT [FK_Staff_Doctor] FOREIGN KEY([docid])
REFERENCES [dbo].[Doctor] ([docno])
GO
ALTER TABLE [dbo].[Staff] CHECK CONSTRAINT [FK_Staff_Doctor]
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD  CONSTRAINT [FK_Staff_Nurse] FOREIGN KEY([nurseid])
REFERENCES [dbo].[Nurse] ([nurseid])
GO
ALTER TABLE [dbo].[Staff] CHECK CONSTRAINT [FK_Staff_Nurse]
GO
ALTER TABLE [dbo].[Stock]  WITH CHECK ADD  CONSTRAINT [FK_Stock_MedicalStore] FOREIGN KEY([medicalstoreid])
REFERENCES [dbo].[MedicalStore] ([medicalstoreid])
GO
ALTER TABLE [dbo].[Stock] CHECK CONSTRAINT [FK_Stock_MedicalStore]
GO
ALTER TABLE [dbo].[Test]  WITH CHECK ADD  CONSTRAINT [FK_Test_Doctor] FOREIGN KEY([docno])
REFERENCES [dbo].[Doctor] ([docno])
GO
ALTER TABLE [dbo].[Test] CHECK CONSTRAINT [FK_Test_Doctor]
GO
/****** Object:  StoredProcedure [dbo].[InpaitentNameId]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[InpaitentNameId]
as 
begin 
select inp.pid , po.pname
from dbo.Inpatient inp 
join patient po
on 
inp.pono = po.pono
end

GO
/****** Object:  StoredProcedure [dbo].[MedicineIdAndName]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[MedicineIdAndName]
as 
begin


select m.medid, m.name
from 
dbo.Medicine m

end

GO
/****** Object:  StoredProcedure [dbo].[OutpaitentNameId]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[OutpaitentNameId]
as 
begin 
select inp.putpid , po.pname
from dbo.outpaitent inp 
join patient po
on 
inp.pono = po.pono
end

GO
/****** Object:  StoredProcedure [dbo].[SPGetAllDeptIdAndName]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[SPGetAllDeptIdAndName]
 
as
 
 
begin 
 
select deptno , deptname
from Department 

end

GO
/****** Object:  StoredProcedure [dbo].[SPGetAllDocotors]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetAllDocotors]
  as 
  begin 
  select *From [AHMS].[dbo].[Doctor]
  end
  

GO
/****** Object:  StoredProcedure [dbo].[SPGetBedRooms]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SPGetBedRooms]
  as 
  begin
  Select b.bedid, b.bedcharges,r.roomcharges,r.type
  FROM AHMS.dbo.Bed b
  join AHMS.dbo.Room r
  on 
  b.bedid = r.roomid
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetCallOnDoc]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SPGetCallOnDoc]
  as 
  begin
  Select c.callid ,c.charges, d.docname
  FROM AHMS.dbo.CallonDoctor c
  join AHMS.dbo.Doctor d
  on 
  c.callid =d.docno
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetDepartmentIdandName]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  create proc [dbo].[SPGetDepartmentIdandName]
  as 
  begin 
  select deptno, deptname From [AHMS].[dbo].Department
  end
  

GO
/****** Object:  StoredProcedure [dbo].[SPGetDeptHosp]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  CREATE proc [dbo].[SPGetDeptHosp]
  as
  begin

  select d.deptno, d.deptname,d.depfac,d.deploc,h.name
  from Department d
  join 
  Hospital h 
  on 
  d.Hospitalid = h.cstno
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetDisInOutPat]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetDisInOutPat]
  as
  begin
Select d.balancecharges,d.testcharges,d.bloodcharges,d.payment,d.dateofdischarge,d.operationscharges,doccharges,d.treatmentadvice,p.pname
from AHMS.dbo.Discharge d
join 
Ahms.dbo.Inpatient inp
on
d.disid = inp.Pid
join 
AHMS.dbo.outpaitent op
on
op.putpid = d.disid
join 
AHMS.dbo.Patient p 
on
p.pono = inp.Pid AND
p.pono = op.putpid
end

GO
/****** Object:  StoredProcedure [dbo].[SPGetDoctDeptHosp]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
  create proc [dbo].[SPGetDoctDeptHosp]
  as
  begin
  select d.docno, d.docname,d.addressdoc,d.qulifications,d.specialization,d.contactno,dept.deptname,h.name
  from Doctor d 
  join 
  Department dept 
  on 
  d.deptno = dept.deptno
  join 
  Hospital h 
  on
  h.cstno= d.hospitalid  
  end
  

GO
/****** Object:  StoredProcedure [dbo].[SPGetHospitalIdandName]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetHospitalIdandName]
  as 
  begin 
  select cstno, name From Hospital
  end
  

GO
/****** Object:  StoredProcedure [dbo].[SPGetLabDocPat]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetLabDocPat]
  as
  begin
  Select p.pname ,l.amount , l.category ,l.dateofLab , d.docname ,l.weightofpatient
  From AHMS.dbo.LabReport l
  join AHMS.dbo.Doctor d
  on
  l.labno = d.docno
  join AHMS.dbo.Patient p
  on 
  l.labno = p.pono
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetLabIdCategry]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetLabIdCategry]
as
begin

select l.labno, l.category
from  dbo.LabReport l

end 


GO
/****** Object:  StoredProcedure [dbo].[SPGetMedicalStoreIdandName]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetMedicalStoreIdandName]
as
begin

select ms.medicalstoreid, ms.name
from MedicalStore ms
end

GO
/****** Object:  StoredProcedure [dbo].[SPGetMedNurRoomLabs]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SPGetMedNurRoomLabs]
  as 
  begin
  
select i.Pid, p.pname,i.advancepayment, r.type , lr.category , m.name as MediName, n.name
from 
Inpatient i 
join 
Patient p 
on 
i.pono = p.pono
join
Room r
on
i.roomno = r.roomid
join 
LabReport lr
on 
i.labno = lr.labno
join 
Medicine m 
on
i.medid = m.medid
join 
Nurse n
on 
i.nurseid = n.nurseid
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetMedStores]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetMedStores]
  as
  begin
  Select med.name as MediName, m.name, m.addressmedialstore
  From AHMS.dbo.MedicalStore m
  join AHMS.dbo.Medicine med
  on
  m.medicalstoreid =med.medid
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetNurseIdandName]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  create proc [dbo].[SPGetNurseIdandName]
  as 
  begin 
  
  select nurseid,name From Nurse
  end
  

GO
/****** Object:  StoredProcedure [dbo].[SPGetOpDocInP]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SPGetOpDocInP]
  as
  begin
  Select o.operationtheaterno, d.docname , o.patientcondition , o.typeofoperation , o.treatment, pat.pname
  From AHMS.dbo.Operation o
  join AHMS.dbo.Doctor d
  on
  o.docno =d.docno
  join 
  Inpatient inp
  on
  o.inpaitentid = inp.Pid
  join 
  Patient pat
  on
  inp.pono = pat.pono
  
  end


GO
/****** Object:  StoredProcedure [dbo].[SPGetOutPLab]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetOutPLab]
  as
  begin
  Select p.pname, l.category ,op.dateofadmit , op.dateofdiscarge , m.name
  From AHMS.dbo.outpaitent op
  join AHMS.dbo.Patient p
  on
  op.putpid = p.pono
  join AHMS.dbo.LabReport l 
  on 
  op.putpid = l.labno 
  join AHMS.dbo.Medicine m
  on 
  op.putpid = m.medid
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetOutPLabPatMed]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SPGetOutPLabPatMed]
  as
  begin
  Select op.putpid ,p.pname, l.category ,op.dateofadmit , op.dateofdiscarge,m.name



  From AHMS.dbo.outpaitent op 

  join AHMS.dbo.Patient p
  on
  op.putpid = p.pono
  join AHMS.dbo.LabReport l
  on
  op.labno = l.labno
  join AHMS.dbo.Medicine m
  on
  op.medid = m.medid
  end


GO
/****** Object:  StoredProcedure [dbo].[SPGetPatDoc]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetPatDoc]
  as
  begin
  Select p.pname, p.addresspatient ,p.age , p.gender , p.city, p.contactno , d.docname
  From AHMS.dbo.Patient p
  join AHMS.dbo.Doctor d
  on
  p.pono = d.docno
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetPatientIdandName]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create proc [dbo].[SPGetPatientIdandName]
  as 
  begin 
  select pono, pname From Patient
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetRegDoc]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetRegDoc]
  as
  begin
  Select r.paymentcharges , d.docname
  From AHMS.dbo.RegularDoctor r
  join AHMS.dbo.Doctor d
  on
  r.id = d.docno
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetRegPat]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SPGetRegPat]
  as
  begin
  Select p.pname ,r.regdate
  From AHMS.dbo.Registeration r
  join AHMS.dbo.Patient p
  on
  r.pno = p.pono
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetRoomIdAndType]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  create proc [dbo].[SPGetRoomIdAndType]
  as 
  begin 
  select [roomid],[type]
  from 
  [AHMS].[dbo].[Room]
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetStaffDocNurses]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SPGetStaffDocNurses]
  as
  begin
  Select d.docname ,d.qulifications as DocQual, n.name,n.qulifications
  From AHMS.dbo.Staff s
  join AHMS.dbo.Doctor d
  on
  s.docid =d.docno
  join AHMS.dbo.Nurse n
  on
  s.nurseid= n.nurseid

  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetStockMedicalStore]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SPGetStockMedicalStore]
  as
  begin
  Select s.medicalstoreid, m.name,m.addressmedialstore,s.quantity
  From AHMS.dbo.Stock s
  join AHMS.dbo.MedicalStore m 
  on 
  s.sno = m.medicalstoreid 
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetStockMeStore]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetStockMeStore]
  as
  begin
  Select m.name,m.addressmedialstore,s.quantity
  From AHMS.dbo.Stock s
  join AHMS.dbo.MedicalStore m
  on
  s.sno = m.medicalstoreid
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetTestDoc]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetTestDoc]
  as
  begin
  select d.docname,t.name, t.amount
  from AHMS.dbo.Test t 
  join AHMS.dbo.Doctor d
  on 
  t.testid = d.docno
  end

GO
/****** Object:  StoredProcedure [dbo].[SPGetWardIdName]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetWardIdName]
as
begin 
select w.wardid,w.wardname
from ward w 
end 

GO
/****** Object:  StoredProcedure [dbo].[SPGetWardRooms]    Script Date: 11/12/2017 11:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SPGetWardRooms]
  as
  begin
  Select r.roomcharges, r.type, r.statusofroom ,w.wardname
  From AHMS.dbo.Ward w
  join AHMS.dbo.Room r
  on
  w.wardid = r.roomid
  end

GO
