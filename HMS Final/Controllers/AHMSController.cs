﻿using HMS_Final;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace HMS_Final.Controllers
{
    public class AHMSController : Controller
    {

        AHMSEntities ent = new AHMSEntities();

        // GET: AHMS
        #region Bed's Data
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ViewBeds()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddBed(int RoomCharges, int RoomID)
        {
            Bed bd = new Bed();
            bd.bedcharges = RoomCharges;
            bd.roomid = RoomID;

            ent.Beds.Add(bd);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetBedData()
        {
            //List<Bed> list = new List<Bed>();

            //var lst = ent.Beds.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Bed bd = new Bed();
            //    bd.bedid = lst[i].bedid;
            //    bd.bedcharges = lst[i].bedcharges;
            //    bd.roomid = lst[i].roomid;
            //    list.Add(bd);
            //}
            var list = ent.SPGetBedRooms().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult EditViewBed( int Id,int  Charges,int RoomID)
        {
            var result = ent.Beds.SingleOrDefault(p =>p.bedid == Id) ;
            if (result != null)
            {
                result.bedid = Id;
                result.bedcharges = Charges;
                result.roomid = RoomID;
                ent.Beds.Attach(result);
                ent.Entry(result).State = EntityState.Modified;
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Updated Successfully" });
            }



            return Json(new { status = "success", message = "Failed to Upload" });
        }

        public ActionResult DeleteBedData(int id)
        {
            var result = ent.Beds.SingleOrDefault(b => b.bedid == id);
            if (result != null)
            {
                ent.Beds.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        public ActionResult GetRoomdIDandType()
        {

            var list = ent.SPGetRoomIdAndType().ToList();
            return Json(new {status="success", message= list });
        }

        #endregion

        #region CallOnDoctor's Data
        public ActionResult CallOnDoctor()
        {
            return View();
        }
        public ActionResult ViewCallOnDoctors()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddCallOnDoctor(int DCharges, int DoctorNo)
        {
            CallOnDoctor doc = new CallOnDoctor();
            doc.charges = DCharges;
            doc.docno = DoctorNo;

            ent.CallOnDoctors.Add(doc);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }

        //for the ajax call to get data 
        public ActionResult GetCallOnDoctorData()
        {
            //List<CallOnDoctor> list = new List<CallOnDoctor>();

            //var lst = ent.CallOnDoctors.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    CallOnDoctor cal = new CallOnDoctor();
            //    cal.callid = lst[i].callid;
            //    cal.charges = lst[i].charges;
            //    cal.docno = lst[i].docno;
            //    list.Add(cal);
            //}
            var list = ent.SPGetCallOnDoc().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult GetAllDocNameAndId()
        {
            var list = ent.SPGetAllDocotors().ToList();
            return Json(new { status = "success", message = list });
        }

        //edit call on doctors 
        public ActionResult EditCallOnDoc(int ID,int Charges, int DocId)
        {
            var result = ent.CallOnDoctors.FirstOrDefault(p => p.callid == ID);
            if (result != null)
            {
                result.charges = Charges;
                result.docno = DocId;
                ent.CallOnDoctors.Attach(result);
                ent.Entry(result).State = EntityState.Modified;
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Updated Successfully" });
            }
            else
                return Json(new { status = "error", message = "Problem in Updating Data" });
        
    }


        public ActionResult DeleteCallOnDocData(int id)
        {
            var result = ent.CallOnDoctors.SingleOrDefault(b => b.callid == id);
            if (result != null)
            {
                ent.CallOnDoctors.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }
        #endregion

        #region Departments's Data
        public ActionResult Department()
        {
            return View();
        }
        public ActionResult ViewDepartments()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddDepartment(string DeptName, string DeptFac, string DeptLoc, int HodpitalID)
        {
            Department dpt = new Department();
            dpt.deptname = DeptName;
            dpt.depfac = DeptFac;
            dpt.deploc = DeptLoc;
            dpt.Hospitalid = HodpitalID;

            ent.Departments.Add(dpt);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllDeptsData()
        {
            //List<Department> list = new List<Department>();

            //var lst = ent.Departments.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Department dpt = new Department();
            //    dpt.deptno = lst[i].deptno;
            //    dpt.deptname = lst[i].deptname;
            //    dpt.deploc = lst[i].deploc;
            //    dpt.depfac = lst[i].depfac;
            //    list.Add(dpt);
            //}
            //return Json(new { status = "success", message = list });
            var list = ent.SPGetDeptHosp().ToList();

            return Json(new { status = "success", message = list });

        }

        public ActionResult DeleteDeptData(int id)
        {
            var result = ent.Departments.SingleOrDefault(b => b.deptno == id);
            if (result != null)
            {
                ent.Departments.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        #endregion

        #region Discharge's Data
        public ActionResult Discharge()
        {
            return View();
        }
        public ActionResult ViewDischarge()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddDischarge(string DischargeDate, string TreatAdvice, int BalCharges, int TestCharges, int BloodCharges, int Payment, int OperationCharges, int DocCharges, int? IndoorPID, int? OutdoorPID)
        {
            DateTime date = DateTime.ParseExact(DischargeDate, "MM/dd/yyyy", null);

            Discharge d = new Discharge();
            d.dateofdischarge = date;
            d.treatmentadvice = TreatAdvice;
            d.balancecharges = BalCharges;
            d.testcharges = TestCharges;
            d.bloodcharges = BloodCharges;
            d.payment = Payment;
            d.operationscharges = OperationCharges;
            d.doccharges = DocCharges;
            if (IndoorPID == 0)
            {
                IndoorPID = null;
            }
            if (OutdoorPID == 0)
            {
                OutdoorPID = null;
            }


            d.indoorpid = IndoorPID;
            d.outdoorpid = OutdoorPID;




            ent.Discharges.Add(d);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetDischargeData()
        {
            var list = ent.SPGetDisInOutPat().ToList();

            return Json(new { status = "success", message = list });
            //List<Discharge> list = new List<Discharge>();

            //var lst = ent.Discharges.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Discharge dis = new Discharge();
            //    dis.disid = lst[i].disid;
            //    dis.balancecharges = lst[i].balancecharges;
            //    dis.bloodcharges = lst[i].bloodcharges;
            //    dis.testcharges = lst[i].testcharges;
            //    dis.payment = lst[i].payment;
            //    dis.dateofdischarge = lst[i].dateofdischarge;
            //    dis.operationscharges = lst[i].operationscharges;
            //    dis.doccharges = lst[i].doccharges;
            //    dis.treatmentadvice = lst[i].treatmentadvice;
            //    list.Add(dis);
            //}
            //return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteDisData(int id)
        {
            var result = ent.Discharges.SingleOrDefault(b => b.disid == id);
            if (result != null)
            {
                ent.Discharges.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }


        #endregion

        #region Doctor's Data

        public ActionResult Doctor()
        {
            return View();
        }
        public ActionResult ViewDoctors()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddDoctor(string DocName, string DocAddress, string DocEdu, string DocSpecialization, string DocPhoneNo, int DocDeptNo, int DocHodpitalID)
        {
            Doctor doc = new Doctor();
            doc.docname = DocName;
            doc.addressdoc = DocAddress;
            doc.qulifications = DocEdu;
            doc.specialization = DocSpecialization;
            doc.contactno = DocPhoneNo;
            doc.deptno = DocDeptNo;
            doc.hospitalid = DocHodpitalID;

            ent.Doctors.Add(doc);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }

        //for the ajax call to get doctors and hospital and department 
        public ActionResult GetAllDoctorsData()
        {
            var lst = ent.SPGetDoctDeptHosp().ToList();

            return Json(new { status = "success", message = lst });

        }

        //ajax for the fetch hospitals data 
        public ActionResult EditDoctors(int Id, string Name, String Address, String Edu, String Specialization, String PhoneNo, int DocDeptNo, int DocHodpitalID)
        {
            var result = ent.Doctors.SingleOrDefault(b => b.docno == Id);
            if (result != null)
            {
               result.docno= Id;
                result.docname = Name;
                result.addressdoc = Address;
                result.qulifications = Edu;
                result.specialization = Specialization;
                result.contactno = PhoneNo;
                result.Department.deptno = DocDeptNo;
                result.Hospital.cstno = DocHodpitalID;
                ent.Doctors.Attach(result);
                ent.Entry(result).State = EntityState.Modified;
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Updated Successfully" });
            }
            else
                return Json(new { status = "error", message = "Problem in Updating Data" });
        }



        public ActionResult DeleteDocData(int id)
        {
            var result = ent.Doctors.SingleOrDefault(b => b.docno == id);
            if (result != null)
            {
                ent.Doctors.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        #endregion

        #region DoctorsANdDept
        public ActionResult GetHopitalDept() {
            var Hosp = ent.SPGetHospitalIdandName().ToList();
            var dept = ent.SPGetDepartmentIdandName().ToList();
            return Json(new { status = "success", Hospita= Hosp, Department = dept });
        }
        #endregion

        #region Hospital's Data

        public ActionResult Hospital()
        {
            return View();
        }
        public ActionResult ViewHospitals()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddHospital(string HName, string HAddress)
        {
            Hospital hsp = new Hospital();
            hsp.name = HName;
            hsp.addressHospital = HAddress;

            ent.Hospitals.Add(hsp);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }

        //for the ajax call to get data 
        public ActionResult GetAllHospitalData()
        {
            List<Hospital> list = new List<Hospital>();

            var listHs = ent.Hospitals.ToList();
            for (int i = 0; i < listHs.Count; i++)
            {
                Hospital hs = new Hospital();
                hs.cstno = listHs[i].cstno;
                hs.name = listHs[i].name;
                hs.addressHospital = listHs[i].addressHospital;
                list.Add(hs);
            }
            return Json(new { status = "success", message = list });
        }
        public ActionResult DeleteHospData(int id)
        {
            var result = ent.Hospitals.SingleOrDefault(b => b.cstno == id);
            if (result != null)
            {
                ent.Hospitals.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }
        //ajax for the fetch hospitals data 
        public ActionResult EditHospital(int Id, string Name, String Address)
        {
            var result = ent.Hospitals.SingleOrDefault(b => b.cstno == Id);
            if (result != null)
            {
                result.cstno = Id;
                result.name = Name;
                result.addressHospital = Address;
                ent.Hospitals.Attach(result);
                ent.Entry(result).State = EntityState.Modified;
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Updated Successfully" });
            }
            else
                return Json(new { status = "error", message = "Problem in Updating Data" });
        }

        #endregion

        #region InPatient's Data
        public ActionResult InPatient()
        {
            return View();
        }
        public ActionResult AllInPatients()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddInPatient(int PatientNo, int AdvancePayment, int RoomID, int LabID, int MedID, int NurseID)
        {
            Inpatient inP = new Inpatient();
            inP.pono = PatientNo;
            inP.advancepayment = AdvancePayment;
            inP.roomno = RoomID;
            inP.labno = LabID;
            inP.medid = MedID;
            inP.nurseid = NurseID;
            ent.Inpatients.Add(inP);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllInPatientData()
        {
            var list = ent.SPGetMedNurRoomLabs().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteInPatientData(int id)
        {
            var result = ent.Inpatients.SingleOrDefault(b => b.Pid == id);
            if (result != null)
            {
                ent.Inpatients.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        //get the all dropdowns of list and all other things
        public ActionResult GetAllDropDownsPNRM()
        {
            var Patientdata = ent.SPGetPatientIdandName().ToList();
            var RoomId = ent.SPGetBedRooms().ToList();
            var LabReportId = ent.SPGetLabIdCategry().ToList();
            var Medid = ent.MedicineIdAndName().ToList();
            var Nurseid = ent.SPGetNurseIdandName().ToList();

            return Json(new { status = "success", Patient= Patientdata ,Room= RoomId ,Lab=LabReportId,
                Medicine = Medid, Nurse = Nurseid

            });
            
        }

        //edit the in patient data 
        public ActionResult EditInPatient(int PatientNo, int AdvancePayment, int RoomID, int LabID, int MedID, int NurseID,int Id)
        {
            var inP = ent.Inpatients.SingleOrDefault(p => p.Pid == Id);
            if(inP != null){ 
            inP.pono = PatientNo;
            inP.advancepayment = AdvancePayment;
            inP.roomno = RoomID;
            inP.labno = LabID;
            inP.medid = MedID;
            inP.nurseid = NurseID;
            inP.Pid = Id;

                ent.Inpatients.Attach(inP);
                ent.Entry(inP).State = EntityState.Modified;
                ent.SaveChanges();


            return Json(new { status = "success", message = "Data Added" });
        }
            return Json(new { status = "success", message = "Failed Added" });
        }

        #endregion

        #region OutPatient's Data
        public ActionResult OutPatient()
        {
            return View();
        }
        public ActionResult AllOutPatients()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddOutPatient(int PatientNo, DateTime DOAdmit, DateTime DODischarge, int LabID, int MedID)
        {
            outpaitent outP = new outpaitent();
            outP.pono = PatientNo;
            outP.dateofadmit = DOAdmit;
            outP.dateofdiscarge = DODischarge;
            outP.labno = LabID;
            outP.medid = MedID;
            ent.outpaitents.Add(outP);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllOutPatientData()
        {
            //List<outpaitent> list = new List<outpaitent>();

            //var lst = ent.outpaitents.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    outpaitent outP = new outpaitent();
            //    outP.putpid = lst[i].putpid;
            //    outP.dateofadmit = lst[i].dateofadmit;
            //    outP.dateofdiscarge = lst[i].dateofdiscarge;
            //    outP.labno = lst[i].labno;

            //    list.Add(outP);
            //}
            var list = ent.SPGetOutPLabPatMed().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteOutPatientData(int id)
        {
            var result = ent.outpaitents.SingleOrDefault(b => b.putpid == id);
            if (result != null)
            {
                ent.outpaitents.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        public ActionResult EditOutPatient(int PatientNo, DateTime DOAdmit, DateTime DODischarge, int LabID, int MedID,int ID)
        {
            var outP = ent.outpaitents.SingleOrDefault(p=>p.putpid == ID);

            if(outP != null)
            { 
            outP.pono = PatientNo;
            outP.dateofadmit = DOAdmit;
            outP.dateofdiscarge = DODischarge;
            outP.labno = LabID;
            outP.medid = MedID;
                ent.outpaitents.Attach(outP);
                ent.Entry(outP).State = EntityState.Modified;
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Added" });
        }
            return Json(new { status = "success", message = "Failed Added" });
        }

        #endregion

        #region LabReport's Data
        public ActionResult LabReport()
        {
            return View();
        }
        public ActionResult AllLabReports()
        {
            return View();
        }
        public ActionResult AddLabReport(int inputAmount, DateTime inputDate, int inputWeight, int PatientNo, string InputCategory, int InputDoctorID)
        {
            LabReport lab = new LabReport();
            lab.amount = inputAmount;
            lab.dateofLab = inputDate;
            lab.weightofpatient = inputWeight;
            lab.pno = PatientNo;
            lab.category = InputCategory;
            lab.docno = InputDoctorID;
            ent.LabReports.Add(lab);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetLabData()
        {
            //List<LabReport> list = new List<LabReport>();

            //var lst = ent.LabReports.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    LabReport lab = new LabReport();
            //    lab.labno = lst[i].labno;
            //    lab.amount = lst[i].amount;
            //    lab.dateofLab = lst[i].dateofLab;
            //    lab.weightofpatient = lst[i].weightofpatient;
            //    lab.pno = lst[i].pno;
            //    lab.category = lst[i].category;
            //    lab.docno = lst[i].docno;
            // 
            // list.Add(lab);
            //}
            var list = ent.SPGetLabDocPat().ToList();

            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteLabData(int id)
        {
            var result = ent.LabReports.SingleOrDefault(b => b.labno == id);
            if (result != null)
            {
                ent.LabReports.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        public ActionResult DropdownLapReport()
        {
            var Patient = ent.SPGetPatientIdandName().ToList();
            var LabReport = ent.SPGetLabIdCategry().ToList();
            var Doc = ent.SPGetAllDocotors().ToList();

            return Json(new { status = "success",  Patient = Patient, LabReport= LabReport, Doc= Doc });

        }


        #endregion

        #region MedicalStore's Data
        public ActionResult MedicalStore()
        {
            return View();
        }
        public ActionResult AllMedicalStores()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddMedicalStore(string MSName, string MSAddress, int MSMedID)
        {
            MedicalStore ms = new MedicalStore();
            ms.name = MSName;
            ms.addressmedialstore = MSAddress;
            ms.medid = MSMedID;

            ent.MedicalStores.Add(ms);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllMedicalStoreData()
        {
            //List<MedicalStore> list = new List<MedicalStore>();

            //var lst = ent.MedicalStores.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    MedicalStore ms = new MedicalStore();
            //    ms.medicalstoreid = lst[i].medicalstoreid;
            //    ms.name = lst[i].name;
            //    ms.addressmedialstore = lst[i].addressmedialstore;
            //    list.Add(ms);
            //}
            var list = ent.SPGetMedStores().ToList();

            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteMediStoreData(int id)
        {
            var result = ent.MedicalStores.SingleOrDefault(b => b.medicalstoreid == id);
            if (result != null)
            {
                ent.MedicalStores.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        #endregion

        #region Medicine's Data
        public ActionResult Medicine()
        {
            return View();
        }
        public ActionResult AllMedicines()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddMedicine(string MedName, DateTime MedExpDate)
        {
            Medicine med = new Medicine();
            med.name = MedName;
            med.expiredatemed = MedExpDate;

            ent.Medicines.Add(med);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllMedicineData()
        {
            List<Medicine> list = new List<Medicine>();

            var lst = ent.Medicines.ToList();
            for (int i = 0; i < lst.Count; i++)
            {
                Medicine med = new Medicine();
                med.medid = lst[i].medid;
                med.name = lst[i].name;
                med.expiredatemed = lst[i].expiredatemed;
                list.Add(med);
            }
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteMedicineData(int id)
        {
            var result = ent.Medicines.SingleOrDefault(b => b.medid == id);
            if (result != null)
            {
                ent.Medicines.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }
        #endregion

        #region Nurse's Data
        public ActionResult Nurse()
        {
            return View();
        }
        public ActionResult AllNurses()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddNurse(string NurName, string NurQualification)
        {
            Nurse nur = new Nurse();
            nur.name = NurName;
            nur.qulifications = NurQualification;

            ent.Nurses.Add(nur);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllNurseData()
        {
            List<Nurse> list = new List<Nurse>();

            var lst = ent.Nurses.ToList();
            for (int i = 0; i < lst.Count; i++)
            {
                Nurse nur = new Nurse();
                nur.nurseid = lst[i].nurseid;
                nur.name = lst[i].name;
                nur.qulifications = lst[i].qulifications;

                list.Add(nur);
            }
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteNurseData(int id)
        {
            var result = ent.Nurses.SingleOrDefault(b => b.nurseid == id);
            if (result != null)
            {
                ent.Nurses.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }
        #endregion

        #region Operation's Data
        public ActionResult Operation()
        {
            return View();
        }
        public ActionResult AllOperations()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddOperation(string Treatment, int docID, string PatientCondition, string OprType, int InPID)
        {
            Operation op = new Operation();

            op.docno = docID;
            op.patientcondition = PatientCondition;
            op.typeofoperation = OprType;
            op.treatment = Treatment;
            var indoorp = ent.Inpatients.SingleOrDefault(p => p.Pid == InPID);
            op.inpaitentid = indoorp.Pid;

            ent.Operations.Add(op);
            try
            {
                ent.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            return Json(new { status = "success", message = "Data Added" });
        }

        //for the ajax call to get data 
        public ActionResult GetAllOperationData()
        {
            //List<Operation> list = new List<Operation>();

            //var lst = ent.Operations.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Operation op = new Operation();
            //    op.operationtheaterno = lst[i].operationtheaterno;
            //    op.treatment = lst[i].treatment;
            //    op.patientcondition = lst[i].patientcondition;
            //    op.typeofoperation = lst[i].typeofoperation;
            //    op.docno = lst[i].docno;
            //    op.inpaitentid = lst[i].inpaitentid;

            //    list.Add(op);
            //}
            var list = ent.SPGetOpDocInP().ToList();

            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteOprData(int id)
        {
            var result = ent.Operations.SingleOrDefault(b => b.operationtheaterno == id);
            if (result != null)
            {
                ent.Operations.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        #endregion

        #region Patient's Data

        public ActionResult Patient()
        {
            return View();
        }
        public ActionResult AllPatients()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddPatient(string Name, int Age, string PhoneNumber, string Addres, string City, string Gender, int docID, string Type)
        {
            Patient p = new Patient();
            p.pname = Name;
            p.age = Age;
            p.contactno = PhoneNumber;
            p.addresspatient = Addres;
            p.city = City;
            p.gender = Gender;
            p.docid = docID;
            bool type = false;
            if (int.Parse(Type) == 1)
            {
                type = true;
            }

            p.InOutCheck = type;
            ent.Patients.Add(p);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllPatientData()
        {
            //List<Patient> list = new List<Patient>();

            //var lst = ent.Patients.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Patient p = new Patient();
            //    p.pono = lst[i].pono;
            //    p.pname = lst[i].pname;
            //    p.age = lst[i].age;
            //    p.contactno = lst[i].contactno;
            //    p.addresspatient = lst[i].addresspatient;
            //    p.city = lst[i].city;
            //    p.gender = lst[i].gender;
            //    p.docid = lst[i].docid;

            //    list.Add(p);
            //}
            var list = ent.SPGetPatDoc().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeletePatientData(int id)
        {
            var result = ent.Patients.SingleOrDefault(b => b.pono == id);
            if (result != null)
            {
                ent.Patients.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }
        #endregion

        #region Registration's Data
        public ActionResult Registration()
        {
            return View();

        }
        public ActionResult AllRegistrations()
        {
            return View();

        }
        //for the ajax call to insert the data 
        public ActionResult AddRegistration(string inputDate, int InputPID)
        {
            Registeration reg = new Registeration();
            reg.regdate =Convert.ToDateTime( inputDate);
            reg.pno = InputPID;

            ent.Registerations.Add(reg);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllRegistrationData()
        {
            //List<Registeration> list = new List<Registeration>();

            //var lst = ent.Registerations.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Registeration reg = new Registeration();
            //    reg.regno = lst[i].regno;
            //    reg.regdate = lst[i].regdate;
            //    reg.regno = lst[i].regno;

            //    list.Add(reg);
            //}
            var list = ent.SPGetRegPat().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteRegData(int id)
        {
            var result = ent.Registerations.SingleOrDefault(b => b.regno == id);
            if (result != null)
            {
                ent.Registerations.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        #endregion

        #region RegularDoc's Data
        public ActionResult RegularDoc()
        {
            return View();
        }
        public ActionResult AllRegularDocs()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddRegularDoc(int PaymentCharges, int DocID)
        {
            RegularDoctor reg = new RegularDoctor();
            reg.paymentcharges = PaymentCharges;
            reg.docno = DocID;

            ent.RegularDoctors.Add(reg);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllRegDoctData()
        {
            //List<RegularDoctor> list = new List<RegularDoctor>();

            //var lst = ent.RegularDoctors.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    RegularDoctor reg = new RegularDoctor();
            //    reg.id = lst[i].id;
            //    reg.paymentcharges = lst[i].paymentcharges;
            //    reg.docno = lst[i].docno;

            //    list.Add(reg);
            //}
            var list = ent.SPGetRegDoc().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteRegDocData(int id)
        {
            var result = ent.RegularDoctors.SingleOrDefault(b => b.id == id);
            if (result != null)
            {
                ent.RegularDoctors.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }

        #endregion

        #region Room's Data
        public ActionResult Room()
        {
            return View();
        }
        public ActionResult AllRooms()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddRoom(string RWardID, string RoomCharges, string RoomStatus, string RoomType)
        {
            Room room = new Room();
            room.wardid = int.Parse(RWardID);
            room.roomcharges = int.Parse(RoomCharges);
            bool status = false;
            if (int.Parse(RoomStatus) == 1)
            {
                status = true;
            }
            room.statusofroom = status;
            room.type = RoomType;

            ent.Rooms.Add(room);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllRoomData()
        {
            //List<Room> list = new List<Room>();
            //var lst = ent.Rooms.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Room room = new Room();
            //    room.roomid = lst[i].roomid;
            //    room.wardid = lst[i].wardid;
            //    room.roomcharges = lst[i].roomcharges;
            //    room.statusofroom = lst[i].statusofroom;

            //    room.type = lst[i].type;

            //    list.Add(room);
            //}
            var list = ent.SPGetWardRooms().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteRoomData(int id)
        {
            var result = ent.Rooms.SingleOrDefault(b => b.roomid == id);
            if (result != null)
            {
                ent.Rooms.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }
        #endregion

        #region Staff's Data
        public ActionResult Staff()
        {
            return View();
        }
        public ActionResult AllStaff()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddStaff(int DocID, int NurseID)
        {
            Staff s = new Staff();
            s.docid = DocID;
            s.nurseid = NurseID;

            ent.Staffs.Add(s);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllStaffData()
        {
            //List<Staff> list = new List<Staff>();
            //var lst = ent.Staffs.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Staff s = new Staff();
            //    s.staffid = lst[i].staffid;
            //    s.docid = lst[i].docid;
            //    s.nurseid = lst[i].nurseid;

            //    list.Add(s);
            //}
            var list = ent.SPGetStaffDocNurses().ToList();
            return Json(new { status = "success", message = list });
        }
        public ActionResult DeleteStaffData(int id)
        {
            var result = ent.Staffs.SingleOrDefault(b => b.staffid == id);
            if (result != null)
            {
                ent.Staffs.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });
        }
        #endregion

        #region Stock's Data
        public ActionResult Stock()
        {
            return View();
        }
        public ActionResult AllStock()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddStock(int Quantity, int MedStoreID)
        {
            Stock st = new Stock();
            st.quantity = Quantity;
            st.medicalstoreid = MedStoreID;

            ent.Stocks.Add(st);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllStockData()
        {
            //List<Stock> list = new List<Stock>();
            //var lst = ent.Stocks.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Stock st = new Stock();
            //    st.quantity = lst[i].quantity;
            //    st.medicalstoreid = lst[i].medicalstoreid;

            //    list.Add(st);
            // }
            var list = ent.SPGetStockMedicalStore().ToList();
            return Json(new { status = "success", message = list });
        }

        //for the ajax call to get data 
        public ActionResult GetAllStockDataIDName()
        {
            //List<Stock> list = new List<Stock>();
            //var lst = ent.Stocks.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Stock st = new Stock();
            //    st.quantity = lst[i].quantity;
            //    st.medicalstoreid = lst[i].medicalstoreid;

            //    list.Add(st);
            // }
            var list = ent.SPGetMedicalStoreIdandName().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteStock(int id)
        {
            var result = ent.Stocks.SingleOrDefault(b => b.sno == id);
            if (result != null)
            {
                ent.Stocks.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });

        }
        #endregion

        #region Test's Data
        public ActionResult Test()
        {
            return View();
        }
        public ActionResult AllTests()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddTest(string Name, int DocID, int Amount)
        {
            Test t = new Test();
            t.name = Name;
            t.docno = DocID;
            t.amount = Amount;

            ent.Tests.Add(t);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllTestData()
        {
            //List<Test> list = new List<Test>();
            //var lst = ent.Tests.ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    Test t = new Test();
            //    t.name = lst[i].name;
            //    t.docno = lst[i].docno;
            //    t.amount = lst[i].amount;
            //    list.Add(t);
            //}
            var list = ent.SPGetTestDoc().ToList();

            return Json(new { status = "success", message = list });
        }
        public ActionResult DeleteTest(int id)
        {
            var result = ent.Tests.SingleOrDefault(b => b.testid == id);
            if (result != null)
            {
                ent.Tests.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });

        }

        #endregion

        #region Ward's Data
        public ActionResult Ward()
        {
            return View();
        }
        public ActionResult AllWards()
        {
            return View();
        }
        //for the ajax call to insert the data 
        public ActionResult AddWard(string WardName)
        {
            Ward w = new Ward();
            w.wardname = WardName;
          

            ent.Wards.Add(w);
            ent.SaveChanges();

            return Json(new { status = "success", message = "Data Added" });
        }
        //for the ajax call to get data 
        public ActionResult GetAllWardData()
        {
            List<Ward> list = new List<Ward>();
            var lst = ent.Wards.ToList();
            for (int i = 0; i < lst.Count; i++)
            {
                Ward w = new Ward();
                w.wardid = lst[i].wardid;
                w.wardname = lst[i].wardname;
             
                list.Add(w);
            }
          //  var list = ent.SPGetWardRooms().ToList();
            return Json(new { status = "success", message = list });
        }


        public ActionResult GetAllWardIdName()
        {
           
              var list = ent.SPGetWardIdName().ToList();
            return Json(new { status = "success", message = list });
        }

        public ActionResult DeleteWard(int id)
        {
            var result = ent.Wards.SingleOrDefault(b => b.wardid == id);
            if (result != null)
            {
                ent.Wards.Remove(result);
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Deleted" });
            }
            else
                return Json(new { status = "error", message = "Problem in Deletion" });

        }

        public ActionResult EditWard(string WardName, int WardRoomID)
        {
            var result = ent.Wards.SingleOrDefault(b => b.wardid == WardRoomID);
            if (result != null)
            {
                result.wardid = WardRoomID;
                result.wardname = WardName;
                ent.Wards.Attach(result);
                ent.Entry(result).State = EntityState.Modified;
                ent.SaveChanges();

                return Json(new { status = "success", message = "Data Updated" });
            }
            else
                return Json(new { status = "error", message = "Problem in Updating Data" });

        }
        #endregion

        #region SendEmmai

        public ActionResult SendEmail(string Name,string Phone, String Email, string Date, String Message) {

            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail.rivohost.com");

            mail.From = new MailAddress("Contactus@gmail.com");
            mail.To.Add(Email);
            mail.Subject = "Test Mail";
            mail.Body = Message;
            mail.Subject = "Conformation of Appointment";
            mail.Body = string.Empty;
            mail.IsBodyHtml = true;
            mail.Body += "<center> </center>";// string.Format(body, model.FromName, model.FromEmail, model.Message);

            mail.Body = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head> <meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> <meta name='viewport' content='width=device-width, initial-scale=1' /> <title></title>   <link href='http://localhost:1207/Content/Email/email.css' rel='stylesheet' /></head><body class='body' style='padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none' bgcolor='#ffffff'><table align='center' cellpadding='0' cellspacing='0' width='100%' height='100%' ><tr><td align='center' valign='top' bgcolor='#ffffff'  width='100%'> <table cellspacing='0' cellpadding='0' width='100%'>  <tr> <td style='border-bottom: 3px solid #3bcdc3;' width='100%'> <center>  </center> </td> </tr>  <tr>  <td background='http://cdn-media-1.lifehack.org/wp-content/files/2013/06/doctor-appointment.jpg' bgcolor='#64594b' valign='top' style='background: url(http://cdn-media-1.lifehack.org/wp-content/files/2013/06/doctor-appointment.jpg) no-repeat center; background-color: #64594b; background-position: center;'><div><center><table cellspacing='0' cellpadding='0' width='530' height='303' class='w320'>  <tr> <td valign='middle' style='vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:left;color:white' class='mobile-center' height='303'> <h1>WELCOME !</h1><br><h2 style='color:white;'>You have got a <br> ContactUs Message from Jinnah Hospital!</h2>   </td> </tr> </table>  </center> </div>  </td>  </tr> <tr> <td valign='top'> <center>&nbsp;  </td> </tr></table></body></html>";
            mail.Body += "<p style='font-size:20px; color:#500050;'>Patient Name : <span style='font-size:15px;'>" + Name + "</span> </p>";// string.Format(body, model.FromName, model.FromEmail, model.Message);
           // mail.Body += "<p style='font-size:20px; color:#500050;'>Doctor Name :</p> <p style='font-size:15px;'>" + drname + "</p>";// string.Format(body, model.FromName, model.FromEmail, model.Message);
            mail.Body += "<p style='font-size:15px;'> Your Appointment has been approved, your check up time is 02 : 00 pm to 05 : 00 pm on" + Date + "</p>" + System.Environment.NewLine;// string.Format(body, model.FromName, model.FromEmail, model.Message);


            SmtpServer.Port = 25;
            SmtpServer.Credentials = new System.Net.NetworkCredential("sidra@alishy.com", "Sidra@123");
            SmtpServer.EnableSsl = false;

            SmtpServer.Send(mail);

            return Json(new { status = "success", message="Mail has been sent!"});

        }

        public ActionResult Contactus(string Name, string Phone, String Email, String Message)
        {

            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail.rivohost.com");

            mail.From = new MailAddress("Contactus@gmail.com");
            mail.To.Add(Email);
            mail.Subject = "Test Mail";
            mail.Body = Message;
            mail.Subject = "Contact Us";
            mail.Body = string.Empty;
            mail.IsBodyHtml = true;
            mail.Body += "<center> </center>";// string.Format(body, model.FromName, model.FromEmail, model.Message);

            mail.Body = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head> <meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> <meta name='viewport' content='width=device-width, initial-scale=1' /> <title></title>   <link href='http://localhost:1207/Content/Email/email.css' rel='stylesheet' /></head><body class='body' style='padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none' bgcolor='#ffffff'><table align='center' cellpadding='0' cellspacing='0' width='100%' height='100%' ><tr><td align='center' valign='top' bgcolor='#ffffff'  width='100%'> <table cellspacing='0' cellpadding='0' width='100%'>  <tr> <td style='border-bottom: 3px solid #3bcdc3;' width='100%'> <center>  </center> </td> </tr>  <tr>  <td background='http://cdn-media-1.lifehack.org/wp-content/files/2013/06/doctor-appointment.jpg' bgcolor='#64594b' valign='top' style='background: url(http://cdn-media-1.lifehack.org/wp-content/files/2013/06/doctor-appointment.jpg) no-repeat center; background-color: #64594b; background-position: center;'><div><center><table cellspacing='0' cellpadding='0' width='530' height='303' class='w320'>  <tr> <td valign='middle' style='vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:left;color:white' class='mobile-center' height='303'> <h1>WELCOME !</h1><br><h2 style='color:white;'>You have got a <br> ContactUs Message from Jinnah Hospital!</h2>   </td> </tr> </table>  </center> </div>  </td>  </tr> <tr> <td valign='top'> <center>&nbsp;  </td> </tr></table></body></html>";
            mail.Body += "<p style='font-size:20px; color:#500050;'>Patient Name : <span style='font-size:15px;'>" + Name + "</span> </p>";// string.Format(body, model.FromName, model.FromEmail, model.Message);
                                                                                                                                           // mail.Body += "<p style='font-size:20px; color:#500050;'>Doctor Name :</p> <p style='font-size:15px;'>" + drname + "</p>";// string.Format(body, model.FromName, model.FromEmail, model.Message);
            mail.Body += "<p style='font-size:15px;'> Your Message:" + Message + "</p>" + System.Environment.NewLine;// string.Format(body, model.FromName, model.FromEmail, model.Message);


            SmtpServer.Port = 25;
            SmtpServer.Credentials = new System.Net.NetworkCredential("sidra@alishy.com", "Sidra@123");
            SmtpServer.EnableSsl = false;

            SmtpServer.Send(mail);

            return Json(new { status = "success", message = "Mail has been sent!" });

        }

        //send mail to someone 

        public ActionResult SendEmailToanyOne(string MailTo, string Subject, String Message)
        {

            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail.rivohost.com");

            mail.From = new MailAddress("attiya@hms.com");
            mail.To.Add(MailTo);
            mail.Body = Message;
            mail.Subject = Subject;
            mail.Body = string.Empty;
            mail.IsBodyHtml = true;
            mail.Body += Message;// string.Format(body, model.FromName, model.FromEmail, model.Message);

           

            SmtpServer.Port = 25;
            SmtpServer.Credentials = new System.Net.NetworkCredential("sidra@alishy.com", "Sidra@123");
            SmtpServer.EnableSsl = false;

            SmtpServer.Send(mail);

            return Json(new { status = "success", message = "Mail has been sent!" });

        }


        #endregion


        #region DischargeDataPatient

        public ActionResult DischargeDataPatient()
        {

            var inlist = ent.InpaitentNameId().ToList();

            var outlist = ent.OutpaitentNameId().ToList();

            return Json(new { status = "success", InPat = inlist, OutPat = outlist });
        }

        #endregion

    }
}