﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS_Final.Controllers
{
    public class GyneeWardController : Controller
    {
        // GET: GyneeWard
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PersonalInfo()
        {
            return View();
        }

        public ActionResult PersonalHistory()
        {
            return View();
        }
        public ActionResult PostOperative()
        {
            return View();

        }
        public ActionResult PreOperative()
        {
            return View();
        }
    }
}