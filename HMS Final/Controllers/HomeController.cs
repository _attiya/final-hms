﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS_Final.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult ServicesView() {

            return View();

        }

        public ActionResult ServicesDental()
        {

            return View();

        }

        public ActionResult ServicesBloodBank()
        {

            return View();

        }

        public ActionResult ServicesMedicalResearch()
        {

            return View();

        }
        public ActionResult ServicesAdvice()
        {

            return View();

        }

        public ActionResult ServicesCouncling()
        {

            return View();

        }

        public ActionResult ContactUs()
        {

            return View();

        }


        public ActionResult AppointmentView()
        {

            return View();

        }


    }
}