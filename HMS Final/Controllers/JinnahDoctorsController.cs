﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS_Final.Controllers
{
    public class JinnahDoctorsController : Controller
    {
        // GET: JinnahDoctors
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AliAnees()
        {
            return View();
        }

        public ActionResult SajjadAli()
        {
            return View();
        }

        public ActionResult AlamNoor()
        {
            return View();
        }

        public ActionResult ATIQRamzan()
        {
            return View();
        }
    }
}