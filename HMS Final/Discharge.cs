//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMS_Final
{
    using System;
    using System.Collections.Generic;
    
    public partial class Discharge
    {
        public int disid { get; set; }
        public Nullable<int> balancecharges { get; set; }
        public Nullable<int> testcharges { get; set; }
        public Nullable<int> bloodcharges { get; set; }
        public Nullable<int> payment { get; set; }
        public Nullable<System.DateTime> dateofdischarge { get; set; }
        public Nullable<int> operationscharges { get; set; }
        public Nullable<int> doccharges { get; set; }
        public string treatmentadvice { get; set; }
        public Nullable<int> indoorpid { get; set; }
        public Nullable<int> outdoorpid { get; set; }
    
        public virtual Inpatient Inpatient { get; set; }
        public virtual outpaitent outpaitent { get; set; }
    }
}
