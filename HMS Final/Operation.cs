//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMS_Final
{
    using System;
    using System.Collections.Generic;
    
    public partial class Operation
    {
        public int operationtheaterno { get; set; }
        public Nullable<int> docno { get; set; }
        public string patientcondition { get; set; }
        public string typeofoperation { get; set; }
        public string treatment { get; set; }
        public Nullable<int> inpaitentid { get; set; }
    
        public virtual Doctor Doctor { get; set; }
        public virtual Inpatient Inpatient { get; set; }
    }
}
