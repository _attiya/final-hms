//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMS_Final
{
    using System;
    
    public partial class SPGetPatDoc_Result
    {
        public string pname { get; set; }
        public string addresspatient { get; set; }
        public Nullable<int> age { get; set; }
        public string gender { get; set; }
        public string city { get; set; }
        public string contactno { get; set; }
        public string docname { get; set; }
    }
}
