﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HMS_Final.Startup))]
namespace HMS_Final
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
